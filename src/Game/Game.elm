-- Menu.elm


module Game.Game exposing (..)

import Utils exposing (..)
import Html as H exposing (Html)
import Html.Attributes as HA
import Html.Events as HE
import String
import Char
import Debug


--

import Game.SearchedWord as SearchedWord
import Game.Attempts as Attempts
import Game.Progress as Progress


type GameState
    = Win
    | Loss
    | Running


type alias Model =
    { searchedWord : SearchedWord.Model
    , attempts : Attempts.Model
    , progress : Progress.Model
    , gameState : Maybe GameState
    }


init : Model
init =
    { searchedWord = SearchedWord.init
    , attempts = Attempts.init
    , progress = Progress.init
    , gameState = Maybe.Nothing
    }



--- Update


type Msg
    = NoOp
    | AttemptMade Char
    | StartGame String
    | StopGame


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            model ! []

        StopGame ->
            init ! []

        StartGame word ->
            let
                _ =
                    Debug.log "Starting with word" word
            in
                { searchedWord = SearchedWord.initWithWord word
                , attempts = Attempts.init
                , progress = Progress.init
                , gameState = Maybe.Just Running
                }
                    ! []

        AttemptMade c ->
            let
                ( searchedWord, wasFound ) =
                    SearchedWord.update model.searchedWord c

                progress =
                    Progress.update model.progress wasFound

                attempts =
                    Attempts.addChar model.attempts c

                gameState =
                    updateGameState searchedWord progress
            in
                { searchedWord = searchedWord
                , attempts = attempts
                , progress = progress
                , gameState = Maybe.Just gameState
                }
                    ! []


updateGameState : SearchedWord.Model -> Progress.Model -> GameState
updateGameState word progress =
    if (SearchedWord.isDone word) then
        Win
    else if (Progress.hasLost progress) then
        Loss
    else
        Running


isRunning : Model -> Bool
isRunning model =
    case model.gameState of
        Just Running ->
            True

        _ ->
            False



--- View


view : Model -> Html Msg
view model =
    H.div [ centerStyle ] <|
        case model.gameState of
            Just Win ->
                [ H.div
                    []
                    [ H.h1
                        [ HA.style [ ( "color", "green" ) ] ]
                        [ H.text "You Won!" ]
                    , H.h3
                        []
                        [ H.text ("The word was " ++ (SearchedWord.word model.searchedWord)) ]
                    ]
                ]

            Just Loss ->
                [ H.div
                    [ HA.style
                        [ ( "float", "left" ) ]
                    ]
                    [ H.text "Used"
                    , Attempts.viewUsedLetters model.attempts
                    ]
                , H.div
                    [ HA.style
                        [ ( "float", "left" )
                        ]
                    ]
                    [ Progress.view model.progress
                    ]
                , H.div
                    []
                    [ H.h1
                        [ HA.style [ ( "color", "red" ) ] ]
                        [ H.text "You Lost!" ]
                    , H.h3
                        []
                        [ H.text ("The word was " ++ (SearchedWord.word model.searchedWord)) ]
                    ]
                ]

            Just Running ->
                [ H.div
                    [ HA.style
                        [ ( "float", "left" ) ]
                    ]
                    [ H.text "Used"
                    , Attempts.viewUsedLetters model.attempts
                    ]
                , H.div
                    [ HA.style
                        [ ( "float", "left" )
                        ]
                    ]
                    [ Progress.view model.progress
                    ]
                , H.div
                    [ HA.style
                        [ ( "clear", "both" )
                        , ( "margin", "0 auto" )
                        ]
                    ]
                    [ SearchedWord.view model.searchedWord
                    ]
                , H.div
                    [ HA.style
                        [ ( "clear", "both" )
                        ]
                    ]
                    [ Attempts.viewKeyboard
                        model.attempts
                        AttemptMade
                    ]
                ]

            Nothing ->
                []



--One div with buttom border per letter


centerStyle =
    HA.style
        [ ( "position", "fixed" )
        , ( "text-align", "center" )
        , ( "top", "10%" )
        , ( "left", "10%" )
        ]



-- Visual Keyboard
