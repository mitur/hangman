-- Progress.elm


module Game.Progress exposing (Model, init, view, update, hasLost)

import Html as H exposing (Html)
import Html.Attributes as HA
import Html.Events as HE


--

import Utils exposing (..)
import Game.Gallow as Gallow


type alias Model =
    Int


init : Model
init =
    2


view : Model -> Html a
view model =
    Gallow.view model


update : Model -> Bool -> Model
update model found =
    if found then
        model
    else
        model + 1


hasLost : Model -> Bool
hasLost model =
    14 < model
