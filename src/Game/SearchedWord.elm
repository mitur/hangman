module Game.SearchedWord
    exposing
        ( Model
        , init
        , initWithWord
        , update
        , isDone
        , isStarted
        , isRunning
        , view
        , word
        )

import Html as H exposing (Html)
import Html.Attributes as HA
import Char
import String


--

import Utils exposing (..)


type alias SearchedChar =
    { char : Char
    , found : Bool
    }


type alias Model =
    List SearchedChar


initWithWord : String -> Model
initWithWord word =
    String.toList word
        |> List.map
            (\c ->
                { char = Char.toUpper c
                , found = False
                }
            )


init : Model
init =
    []


isDone : Model -> Bool
isDone model =
    if 0 < List.length model then
        List.all (\sc -> sc.found) model
    else
        False


isStarted : Model -> Bool
isStarted model =
    0 < List.length model


word : Model -> String
word model =
    model
        |> List.map (\sc -> sc.char)
        |> String.fromList


isRunning : Model -> Bool
isRunning model =
    (not (isDone model)) && (isStarted model)


update : Model -> Char -> ( Model, Bool )
update model c =
    if List.any (\sc -> sc.char == c) model then
        List.map
            (\sc ->
                if sc.char == c then
                    { sc | found = True }
                else
                    sc
            )
            model
            & True
    else
        model & False


view : Model -> Html a
view model =
    model
        |> List.map letterBox
        |> H.div []


letterBox : SearchedChar -> Html a
letterBox sc =
    H.div [ charBoxStyle ] <|
        if sc.found then
            [ String.fromList [ sc.char ] |> H.text ]
        else
            []


charBoxStyle : H.Attribute a
charBoxStyle =
    HA.style
        [ ( "border-bottom-width", "2px" )
        , ( "border-bottom-style", "solid" )
        , ( "border-bottom-color", "black" )
        , ( "float", "left" )
        , ( "display", "inline-block" )
        , ( "margin", "10px" )
        , ( "width", "20px" )
        , ( "height", "20px" )
        ]
