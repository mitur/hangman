{--Gallow.elm
    Contains all functionality to draw the Gallow
    and the hangman.
    This is done by drawing n parts of the entire
    Gallow Svg.
--}


module Game.Gallow exposing (view)

import Html as H exposing (Html)
import Html.Attributes as HA
import List
import Svg exposing (Svg, node, circle, line, rect, ellipse, svg, polyline, animate)
import Svg.Attributes exposing (..)
import Tuple exposing (first, second)
import Utils exposing (zip)


{--Draws the Gallow, n is the progress.
    I.e 1 would mean only the hill is drawn.
--}


type alias Point =
    ( Int, Int )


pointsToStrings : Point -> Point -> ( ( String, String ), ( String, String ) )
pointsToStrings start stop =
    ( ( (first start) |> toString
      , (second start) |> toString
      )
    , ( (first stop) |> toString
      , (second stop) |> toString
      )
    )


pointPairToStrings : ( Point, Point ) -> ( ( String, String ), ( String, String ) )
pointPairToStrings pair =
    ( ( (pair |> first |> first) |> toString
      , (pair |> first |> second) |> toString
      )
    , ( (pair |> second |> first) |> toString
      , (pair |> second |> second) |> toString
      )
    )


gheight =
    400


view : Int -> Html a
view n =
    List.take n gallow
        -- gallow
        |>
            H.div
                [ HA.style
                    [ ( "position", "relative" )
                    , ( "width", "420px" )
                    , ( "height", "420px" )
                    ]
                ]

looseText : Html a
looseText =
    toHtml <|
        Svg.text_
            [ x "10"
            , y "200"
            , fontSize "40"
            , stroke "red"
            , fill "red"
            , transform "rotate(-45 200, 200)"
            ]
            [ Svg.text "YOU LOOSE!" ]


toHtml : Svg a -> Html a
toHtml s =
    listToHtml [ s ]


listToHtml : List (Svg a) -> Html a
listToHtml lst =
    H.div
        [ HA.style
            [ ( "position", "absolute" )
            , ( "top", "0px" )
            ]
        ]
        [ svg
            [ viewBox "0 0 400 400", width "400", height "400" ]
            lst
        ]


gallow : List (Html a)
gallow =
    [ toHtml <|
        rect
            [ x "1", width "398px", height "399", lineColor, fill "transparent" ]
            []
      -- Hill
    , toHtml <|
        ellipse
            [ cx "125"
            , cy "400"
            , ry "100"
            , rx "100"
            , stroke "#000000"
            ]
            []
      -- Vertical line
    , animLine ( 125, 300 ) ( 125, 50 ) |> toHtml
      -- Horizontal Top and support
    , animLine ( 125, 50 ) ( 300, 50 ) |> toHtml
    , animLine ( 125, 100 ) ( 200, 50 ) |> toHtml
      -- Rope
    , animLine ( 300, 50 ) ( 300, 123 ) |> toHtml
    , toHtml <|
        circle
            [ cx "320", cy "135", r "0", lineColor, fill "transparent" ]
            [ animate [ attributeName "r", from "0", to "25", fill "freeze", animTimeAttr ] []
            ]
    , animLine ( 300, 150 ) ( 300, 250 ) |> toHtml
    , animLine ( 300, 250 ) ( 350, 300 ) |> toHtml
    , animLine ( 300, 250 ) ( 250, 300 ) |> toHtml
    , animLine ( 300, 175 ) ( 250, 220 ) |> toHtml
    , animLine ( 300, 175 ) ( 350, 220 ) |> toHtml
    , circle [ cx "315", cy "133", r "3", lineColor ] [] |> toHtml
    , circle [ cx "330", cy "140", r "3", lineColor ] [] |> toHtml
    , animLine ( 310, 140 ) ( 320, 150 ) |> toHtml
    ]


animLine : Point -> Point -> Svg a
animLine start stop =
    let
        ( ( startX, startY ), ( stopX, stopY ) ) =
            pointsToStrings start stop
    in
        line
            [ x1 startX
            , y1 startY
            , x2 startX
            , y2 startY
            , lineColor
            , lineWidth
            ]
            [ animate
                [ attributeName "x2"
                , from startX
                , to stopX
                , fill "freeze"
                , animTimeAttr
                ]
                []
            , animate
                [ attributeName "y2"
                , from startY
                , to stopY
                , fill "freeze"
                , animTimeAttr
                ]
                []
            ]


animLines : List ( Point, Point ) -> List (Svg a)
animLines ppLst =
    let
        zipped =
            List.length ppLst
                |> List.range 0
                |> zip ppLst
    in
        zipped
            |> List.map (\( ( p1, p2 ), fac ) -> animLineDelayed p1 p2 fac)


animLineDelayed : Point -> Point -> Int -> Svg a
animLineDelayed start stop delayFac =
    let
        ( ( startX, startY ), ( stopX, stopY ) ) =
            pointsToStrings start stop
    in
        line
            [ x1 startX
            , y1 startY
            , x2 startX
            , y2 startY
            , lineColor
            , lineWidth
            ]
            [ animate
                [ attributeName "x2"
                , from startX
                , to stopX
                , fill "freeze"
                , animTimeAttr
                ]
                []
            , animate
                [ attributeName "y2"
                , from startY
                , to stopY
                , fill "freeze"
                , animTimeAttr
                ]
                []
            ]


lineWidth : Svg.Attribute a
lineWidth =
    strokeWidth "4"


lineColor : Svg.Attribute a
lineColor =
    stroke "#000000"


animTimeAttr : Svg.Attribute a
animTimeAttr =
    dur "0.3s"


animStartAttr : Int -> Svg.Attribute a
animStartAttr delayFac =
    let
        difstr =
            delayFac
                |> toFloat
                |> (*) 0.3
                |> toString
    in
        difstr ++ "s" |> begin

