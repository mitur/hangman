-- Attempts.elm
{--
 Module Attempts keeps track of, and can display
 all letters attempted by user.
--}


module Game.Attempts
    exposing
        ( Model
        , init
        , addChar
        , contains
        , viewKeyboard
        , viewUsedLetters
        )

import Html as H exposing (Html)
import Html.Attributes as HA
import Html.Events as HE
import String


--

import Utils exposing (..)


type alias Model =
    List Char


init : Model
init =
    []


addChar : Model -> Char -> Model
addChar model c =
    c :: model


contains : Model -> Char -> Bool
contains model c =
    List.any (\attempt -> attempt == c) model


viewUsedLetters : Model -> Html a
viewUsedLetters model =
    List.map usedLetter model
        |> H.div []


usedLetter : Char -> Html a
usedLetter c =
    H.div
        []
        [ String.fromList [ c ] |> H.text ]


viewKeyboard : Model -> (Char -> a) -> Html a
viewKeyboard model msg =
    allChars
        |> List.map (keyboardChar model msg)
        |> H.div
            [ HA.style
                []
            ]


keyboardChar : Model -> (Char -> a) -> Char -> Html a
keyboardChar model msg c =
    let
        disabled =
            List.any (\tc -> tc == c) model
    in
        H.div
            [ HA.style
                [ ( "float", "left" )
                , ( "display", "table-cell" )
                , ( "text-align", "center" )
                , ( "vertical-align", "middle" )
                , ( "height", "40px" )
                , ( "width", "40px" )
                ]
            ]
            [ H.button
                [ HA.style
                    [ ( "width", "40px" )
                    , ( "height", "40px" )
                    ]
                , HA.disabled disabled
                , HE.onClick <| msg c
                ]
                [ H.text <| String.fromList [ c ] ]
            ]


allChars : List Char
allChars =
    String.toList "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
