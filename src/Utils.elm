module Utils exposing ((&), zip)


(&) : a -> b -> ( a, b )
(&) a b =
    ( a, b )


zip : List a -> List b -> List ( a, b )
zip lst1 lst2 =
    case ( lst1, lst2 ) of
        ( x :: xs, y :: ys ) ->
            ( x, y ) :: (zip xs ys)

        ( _, _ ) -> -- Slanga-bord-emoji
            []



listLength : List a -> Int
listLength lst =
    case lst of
        _ :: restOfList ->
            1 + (listLength restOfList)

        emptyList ->
            0

