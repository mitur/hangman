-- Main.elm


module Main exposing (..)

import Char
import Debug
import Game.Game as Game
import Html as H exposing (Html)
import Html.Attributes as HA
import Html.Events as HE
import Http
import Json.Decode
import Keyboard


main =
    H.program
        { init = ( model, Cmd.none )
        , view = view
        , subscriptions = subscriptions
        , update = update
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Keyboard.presses KeyPressed



-- Model


type alias Model =
    { game : Game.Model
    , wordLength : Int
    , fetching : Bool
    , fetchError : Maybe String
    }


model =
    { game = Game.init
    , wordLength = 5
    , fetching = False
    , fetchError = Maybe.Nothing
    }



-- Update


type Msg
    = NoOp
    | GameMsg Game.Msg
    | KeyPressed Keyboard.KeyCode
    | StartButtonClicked
    | StopButtonClicked
    | LengthIncreasedClicked
    | LengthDecreasedClicked
    | NewWordReceived (Result Http.Error String)


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            model ! []

        StartButtonClicked ->
            ({ model
                 | fetching = True
             }, fetchRandomWord model.wordLength)


        StopButtonClicked ->
            update
                (GameMsg (Game.StopGame))
                model

        GameMsg gameMsg ->
            let
                ( newGame, gameCmd ) =
                    Game.update gameMsg model.game
            in
                { model | game = newGame } ! [ Cmd.map GameMsg gameCmd ]

        KeyPressed keyCode ->
            update
                (GameMsg (Game.AttemptMade <| Char.fromCode keyCode))
                model

        LengthIncreasedClicked ->
            { model
                | wordLength = model.wordLength + 1 |> min 20
            }
                ! []

        LengthDecreasedClicked ->
            { model
                | wordLength = model.wordLength - 1 |> max 4
            }
                ! []


        NewWordReceived (Ok newWord) ->
            ({ model
            | fetching = False
            }
                 |> update (GameMsg (Game.StartGame newWord)))


        NewWordReceived (Err _) ->
            { model
                 | fetching = False
                 , fetchError = Maybe.Just "Det sket sig"
             }
                 ! []


-- View


view : Model -> Html Msg
view model =
    if model.fetching then
        H.div
            []
            [ H.img
                [ HA.src "loading.gif" ]
                []
            ]
    else
        let
            isRunning =
                Game.isRunning model.game
        in
            H.div
                []
                [ controlsView isRunning
                , lengthControls isRunning model.wordLength
                , H.map GameMsg <| Game.view model.game
                ]


controlsView : Bool -> Html Msg
controlsView isRunning =
    H.div [ HA.style [ ( "float", "left" ) ] ] <|
        if isRunning then
            [ stopButton ]
        else
            [ startButton ]


lengthControls : Bool -> Int -> Html Msg
lengthControls isRunning wordLen =
    H.div [ HA.style [ ( "float", "left" ) ] ] <|
        if isRunning then
            []
        else
            [ H.text "Word length: "
            , toString wordLen |> H.text
            , H.button
                [ HE.onClick LengthDecreasedClicked ]
                [ H.text "-" ]
            , H.button
                [ HE.onClick LengthIncreasedClicked ]
                [ H.text "+" ]
            ]


startButton : Html Msg
startButton =
    H.button
        [ HE.onClick StartButtonClicked ]
        [ H.text "Start" ]


stopButton : Html Msg
stopButton =
    H.button
        [ HE.onClick StopButtonClicked ]
        [ H.text "Stop" ]



fetchRandomWord : Int -> Cmd Msg
fetchRandomWord wordLen =
    fetch wordLen
        |> Http.send NewWordReceived


wordDecoder : Json.Decode.Decoder String
wordDecoder =
    Json.Decode.field "word" Json.Decode.string


fetch : Int -> Http.Request String
fetch wordLen =
   Http.request
        { method = "GET"
        , headers = [ Http.header "X-Mashape-Key" "1uluTAFVyamshwzx4H2LKMOnoVpqp1fhphojsnYcnQ6il802it" ]
        , url = url wordLen
        , body = Http.emptyBody
        , expect = Http.expectJson wordDecoder
        , timeout = Nothing
        , withCredentials = False
        }


url : Int -> String
url wordLen =
    "https://wordsapiv1.p.mashape.com/words/?letters=" ++ (toString wordLen) ++ "&random=true"

