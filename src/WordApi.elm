module WordApi exposing (fetchRandomWord)

import Http
import Json.Decode


fetchRandomWord : Int -> Cmd Msg
fetchRandomWord wordLen =
    fetch wordLen
        |> Http.send NewWordReceived


wordDecoder : Json.Decode.Decoder String
wordDecoder =
    Json.Decode.field "word" Json.Decode.string


fetch : Int -> Http.Request String
fetch wordLen =
   Http.request
        { method = "GET"
        , headers = [ Http.header
                          "X-Mashape-Key"
                          "1uluTAFVyamshwzx4H2LKMOnoVpqp1fhphojsnYcnQ6il802it"
                    ]
        , url = url
        , body = Nothing
        , expect = Http.expectJson wordDecoder
        , timeout = Nothing
        , withCredentials = False
        }


    Http.get (url wordLen) wordDecoder -- Request String



url : Int -> String
url wordLen =
    "https://wordsapiv1.p.mashape.com/words/?letters=" ++ (toString wordLen) ++ "&random=true"

